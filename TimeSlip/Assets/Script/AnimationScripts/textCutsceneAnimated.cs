﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class textCutsceneAnimated : MonoBehaviour {
    public float letterTime = 0.001f;
    public string fullText;
    public string currentText = "";
    public Text cutsceneText;

	// Use this for initialization
	void Start () {
        cutsceneText = this.GetComponent<Text>();
        //fullText = cutsceneText.text;
        //cutsceneText.text = "";


    }
	
    public void startAnimated()
    {
        
        StartCoroutine(animatedText());
    }
    IEnumerator animatedText() {


        for (int i = 0; i < fullText.Length; i++)
        {
            currentText = fullText.Substring(0,i);
            cutsceneText.text = currentText;
            yield return new WaitForSeconds(letterTime);
        }

    }
}
