﻿using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public Transform Target;
    public float smoothness = 0.125f;
    public Vector3 offset;

    public Vector3 defaultCamAngles;
    float jump1RotX = 57f;
    float defaultRotx = 10f;

    float offsetX;
    PlayerCharacter1 pl;
    // Use this for initialization

    void Start() {
        pl = GameObject.FindObjectOfType<PlayerCharacter1>();
    }

    // Update is called once per frame
    void FixedUpdate() {

        FollowPlayer();
        DoubleJumpCam();


    }

    public void updateTarget() {
        if (Target.parent != null) {
            Target = Target.parent;
        }
    }

    public void FollowPlayer()
    {
        Vector3 expectedPosition = Target.position + offset;
        Vector3 smoothnessPosition = Vector3.Lerp(transform.position, expectedPosition, smoothness); //linear interpolation
        transform.position = smoothnessPosition;
    }
    public void DoubleJumpCam()
    {
        if (pl.jumplimit == 0)
        {
            if (transform.eulerAngles.x > 10f)
                transform.Rotate(new Vector3(Time.deltaTime * -130f, 0f, 0f));
            else if (transform.eulerAngles.x < 10f)
            {
                transform.eulerAngles = defaultCamAngles;
            }
        }

        else if (pl.jumplimit >= 2)
        {
            if (transform.eulerAngles.x < 57f)
                transform.Rotate(new Vector3(Time.deltaTime * 100f, 0f, 0f));
            else if (transform.eulerAngles.x > 57f)
            {
                transform.eulerAngles = new Vector3(57f, -180f, 0f); ;
            }


        }

        else
        {
            return;
        }



    }
}
