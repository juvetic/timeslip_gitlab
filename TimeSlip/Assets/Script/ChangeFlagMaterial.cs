﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeFlagMaterial : MonoBehaviour {
    Renderer rend;
    public GameObject PinkFlag;
    public GameObject GreenFlag;
    // Use this for initialization
    void Start () {
        PinkFlag.SetActive(true);
        GreenFlag.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
	}

    public void theCharacterIsOnCheckPoint() {
        PinkFlag.SetActive(false);
        GreenFlag.SetActive(true);
    }
}
