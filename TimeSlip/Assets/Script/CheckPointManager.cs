﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointManager : MonoBehaviour {
    public GameObject[] flag;
    bool playerIsOn = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Player") {
            if (!playerIsOn) {
                FindObjectOfType<SoundManager>().Play("CheckPoint");
            }
            for (int i = 0; i < flag.Length; i++) {
                if (flag[i]) {
                    flag[i].GetComponent<ChangeFlagMaterial>().theCharacterIsOnCheckPoint();
                }
                
            }
            playerIsOn = true;
        }
    }
}
