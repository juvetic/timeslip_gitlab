﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public int currentLevel;
    public GameObject failedUI;
    public GameObject LevelEndedUI;
    public GameObject PauseUI;
    public GameObject playerCharacter;
    public GameObject[] DisappearGameObject;
    
    public float ObjectMovingTime = 1.0f;
    public bool paused = false;

    public bool TimeStop = false;
    public bool CalculateScore = false;
    public float timeCount = 0.0f;
    public float StandardScore = 10000.0f;
    public float BonusScore = 3000.0f;
    public float Score = 0.0f;
    public float totalScore = 0.0f;
    float highScore = 0.0f;

    public GameObject ScoreText;
    public Text highScoreText;

    // Use this for initialization
    void Start () {
        if (PlayerPrefs.HasKey("HighScore") == false)
        {
            PlayerPrefs.SetFloat("HighScore", highScore);
        }
        else {
            highScore =  PlayerPrefs.GetFloat("HighScore");
        }
        TimeStop = false;
        failedUI.SetActive(false);
        LevelEndedUI.SetActive(false);
        PauseUI.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        if (TimeStop == false)
        {
            timeCount += Time.deltaTime;
        }
        else {
            if (CalculateScore == true) {
                calculateScore();
                CalculateScore = false;
            }
            
        }
        if (playerCharacter.activeSelf == false)
        {
            failedUI.SetActive(true);
            Time.timeScale = 0;
        }
        else {
            failedUI.SetActive(false);
            Time.timeScale = 1;
        }
        if (failedUI.activeSelf == true) {
            if (Input.anyKeyDown) {
                BonusScore -= 1000.0f;
                if (BonusScore <= 0.0f)
                {
                    BonusScore = 0.0f;
                }
                playerCharacter.SetActive(true);
                playerCharacter.GetComponent<PlayerCharacter1>().respawn();
            }
        }
        PausePress();
	}

    public void calculateScore() {
        Score = StandardScore - (timeCount * 10.0f);
        if (Score <= 0) {
            Score = 0;
        }
        totalScore = Score + BonusScore;
        if (totalScore > highScore) {
            highScore = totalScore;
            PlayerPrefs.SetFloat("HighScore", highScore);
        }
        ScoreText.GetComponent<Text>().text = "Score: " + ((int)totalScore).ToString();
        highScoreText.text = "(Best: "+ ((int)highScore).ToString() + ")";
    }

    public void PausePress()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!paused)
            {
                PauseUI.SetActive(true);
                Time.timeScale = 0;
                paused = true;
            }
            else
            {
                PauseUI.SetActive(false);
                Time.timeScale = 1;
                paused = false;

            }
        }
    }

    public void ResumePress()
    {
        PauseUI.SetActive(false);
        Time.timeScale = 1;
        paused = false;
    }
    public void RestartPress()
    {
        if (currentLevel == 1)
        {
            SceneManager.LoadScene("Stage 1-1");
        }
        else if (currentLevel == 2)
        {
            SceneManager.LoadScene("Stage 1-2");
        }else{
            SceneManager.LoadScene("Stage 1-3");
        }
        
    }
    public void LVSelectionPress()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("StageSelectScene");
    }
    public void MenuPress()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MenuScene");
    }

    public void GoToNextStage()
    {
        Time.timeScale = 1;
        //currentLevel++;
        if (currentLevel == 2)
        {
            SceneManager.LoadScene("Stage 1-3");
        } else if (currentLevel == 1) {
            SceneManager.LoadScene("Stage 1-2");
        }
        else {
            SceneManager.LoadScene("MenuScene");
        }
        
    }


    }
