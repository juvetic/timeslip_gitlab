﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicateSizePlatform : MonoBehaviour {
    public float BiggestSize = 200.0f;
    public float DefaultBiggestSize = 200.0f;
    public GameObject Player;
	// Use this for initialization
	void Start () {
        Player = GameObject.FindGameObjectWithTag("Player");
        //DefaultBiggestSize = Player.GetComponent<PlayerCharacter1>().BigSize;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnCollisionStay(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            col.gameObject.GetComponent<PlayerCharacter1>().BigSize = BiggestSize;
        }
    }

    void OnCollisionExit(Collision col)
    {
        if (col.gameObject.tag == "Player")
        {
            
            col.gameObject.GetComponent<PlayerCharacter1>().BigSize = DefaultBiggestSize;
        }
    }
}
