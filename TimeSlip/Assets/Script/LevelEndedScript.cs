﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelEndedScript : MonoBehaviour {

    public Text scoreText;
    public Text levelNameText;
    public Text HighScoreText;
    public GameObject GemBG;
    public GameObject Gem;
    public Animator anim;
    // Use this for initialization
    void Start ()
    {
        scoreText.gameObject.SetActive(false);
        levelNameText.gameObject.SetActive(false);
        HighScoreText.gameObject.SetActive(false);
        switch (SceneManager.GetActiveScene().name)
        {
            case "Stage 1-1":
                levelNameText.text = "STAGE 1 DONE";
                break;
            case "Stage 1-2":
                levelNameText.text = "STAGE 2 DONE";
                break;
            default:
                levelNameText.text = "LAST STAGE DONE";
                break;
        }
	}
    public void ActiveText()
    {
        StartCoroutine(TextSequence());
    }

    public void CalculateScore()
    {

    }

    IEnumerator TextSequence()
    {
        yield return new WaitForSeconds(0.3f);
        levelNameText.gameObject.SetActive(true);
        FindObjectOfType<SoundManager>().Play("Error");
        yield return new WaitForSeconds(0.3f);
        scoreText.gameObject.SetActive(true);
        FindObjectOfType<SoundManager>().Play("Error");
        yield return new WaitForSeconds(0.3f);
        HighScoreText.gameObject.SetActive(true);
        anim.Play("LevelEnded-GemsDetail", -1, 0f);
        FindObjectOfType<SoundManager>().Play("Error");
    }

}
