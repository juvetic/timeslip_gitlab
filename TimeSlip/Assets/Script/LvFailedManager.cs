﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


public class LvFailedManager : MonoBehaviour {
    public Slider PlayerSlider;
    public LevelDistance levelDistance_;
      
	// Use this for initialization
	void Start () {
        levelDistance_ = GameObject.FindObjectOfType<LevelDistance>();

	}
	
	// Update is called once per frame
	void Update () {
        PlayerSlider.value = levelDistance_.percentage;
	}
}
