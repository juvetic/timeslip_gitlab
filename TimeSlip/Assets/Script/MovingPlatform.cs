﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour {
    public GameObject plaformUp;
    public GameObject plaformDown;
    public float speed = 2200.0f;
    public float OriginalSpeed = 2200.0f;
    public string type = "UpDown";
    public bool movedown = true;
    public bool moveLeft = true;
    public bool moveBack = true;
    public GameObject GameManager;
    public float Waittime = 0.0f;
    public bool AffectedBySize = true;
    float slowdownTime = 1.0f;

    // Use this for initialization
    void Start () {
        GameManager = GameObject.Find("GameManager");
        
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (AffectedBySize == true)
        {
            slowdownTime = GameManager.GetComponent<GameManager>().ObjectMovingTime;
        }
        else {
            slowdownTime = 1.0f;
        }
        if (type == "UpDown")
        {
            if (movedown == true)
            {
                gameObject.transform.Translate(0.0f, -speed * (Time.deltaTime / slowdownTime), 0.0f);
            }
            else
            {
                gameObject.transform.Translate(0.0f, speed * (Time.deltaTime / slowdownTime), 0.0f);
            }

            if (gameObject.transform.position.y >= plaformUp.transform.position.y)
            {
                movedown = true;
            }
            if (gameObject.transform.position.y < plaformDown.transform.position.y)
            {
                movedown = false;
            }
        }
        else if (type == "LeftRight")
        {
            if (moveLeft == true)
            {
                gameObject.transform.Translate(-speed * (Time.deltaTime / slowdownTime), 0.0f, 0.0f);
            }
            else
            {
                gameObject.transform.Translate(speed * (Time.deltaTime / slowdownTime), 0.0f, 0.0f);
            }

            if (gameObject.transform.position.x >= plaformUp.transform.position.x)
            {
                moveLeft = true;
            }
            if (gameObject.transform.position.x < plaformDown.transform.position.x)
            {
                moveLeft = false;
            }
        } else if (type == "ForwardBackwardAndWait") {
            Waittime -= Time.deltaTime;
            speed -= 10.0f;
            if (Waittime <= 0.0f) {
                if (moveBack == true)
                {
                    gameObject.transform.Translate(0.0f, 0.0f, -speed * (Time.deltaTime / slowdownTime));
                }
                else
                {
                    gameObject.transform.Translate(0.0f, 0.0f, speed * (Time.deltaTime / slowdownTime));
                }
            }

            

            if (gameObject.transform.position.z >= plaformUp.transform.position.z)
            {
                if (moveBack == false) {
                    Waittime = 1.0f;
                }
                speed = OriginalSpeed;
                moveBack = true;
                
            }
            if (gameObject.transform.position.z < plaformDown.transform.position.z)
            {
                if (moveBack == true)
                {
                    Waittime = 1.0f;
                }
                speed = OriginalSpeed;
                moveBack = false;
            }
        }
        else {
            if (moveBack == true)
            {
                gameObject.transform.Translate(0.0f, 0.0f, -speed * (Time.deltaTime / slowdownTime));
            }
            else
            {
                gameObject.transform.Translate(0.0f, 0.0f, speed * (Time.deltaTime / slowdownTime));
            }

            if (gameObject.transform.position.z >= plaformUp.transform.position.z)
            {
                moveBack = true;
            }
            if (gameObject.transform.position.z < plaformDown.transform.position.z)
            {
                moveBack = false;
            }
        }
        
    }

}
