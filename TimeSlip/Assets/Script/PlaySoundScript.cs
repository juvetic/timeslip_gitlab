﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundScript : MonoBehaviour {

    // Use this for initialization
    public void PointAtBtn()
    {
        FindObjectOfType<SoundManager>().Play("Point"); ;
    }
    public void CickBtnError()
    {
        FindObjectOfType<SoundManager>().Play("Error"); ;
    }
    public void ClickBtn()
    {
        FindObjectOfType<SoundManager>().Play("Click"); ;
    }

    public void Check()
    {
        FindObjectOfType<SoundManager>().Play("CheckPoint"); ;
    }
}
