﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter1 : MonoBehaviour
{
    public float Jumpforce = 100.0f;
    public float DefaultJumpForce = 2680.0f;
    public float MassDefault = 0.15f;
    public float BigSize = 100.0f;
    public float DefaultSize = 20.0f;
    public float SmallSize = 1.0f;
    public float DefaultDrag = 1.0f;
    public float DefaultSpeed = 0.25f;
    public float DefaultRunSpeed = 0.35f;
    public int DefaultFieldOfView = 60;
    public static int checkPoint = 0;
    public static Vector3 spawnPosition;
    public Vector3 OriginalSpawnPosition;

    public int jumplimit = 0;

    public GameObject camera;
    public GameObject BlobShadow;
    public GameObject VictoryUI;
    public GameObject StartingPoint;
    public GameObject GameManager;
    float PositionBeforeJumping = 0;
    float sizeBeforeJumping = 0;
    bool isGround = false;
    bool OnAir = false;
    bool FPS = false;
    bool jumping = false;
    public PlaySoundScript PlaySound;
    public CameraFollow CamFollow;
    Rigidbody rb;
    AudioSource m_MyAudioSource;
    float shrinkPitch = 1.5f;
    float walkPitch = 1.0f;
    public Animator m_Animator;

    public GameObject transformation_Big_Effect;
    public GameObject transformation_Small_Effect;

    bool disableInput = false;

    // Use this for initialization
    void Start()
    {
        this.transform.localScale = new Vector3(DefaultSize, DefaultSize, DefaultSize);
        Jumpforce = DefaultJumpForce;

        Time.timeScale = 0.8f;
        rb = GetComponent<Rigidbody>();
        m_MyAudioSource = GetComponent<AudioSource>();
        camera = GameObject.Find("PlayerCamera");
        GameManager = GameObject.Find("GameManager");
        PlaySound = GameObject.FindObjectOfType<PlaySoundScript>();
        CamFollow = GameObject.FindObjectOfType<CameraFollow>();

        m_Animator = GetComponent<Animator>();
    }

    public void respawn()
    {
        this.transform.localScale = new Vector3(DefaultSize, DefaultSize, DefaultSize);
        Jumpforce = DefaultJumpForce;
        if (checkPoint == 0)
        {
            gameObject.transform.position = OriginalSpawnPosition;
        }
        else
        {
            gameObject.transform.position = spawnPosition;
        }
        Time.timeScale = 1.0f;
    }

    void Update()
    {
        Jump();
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        JumpForceController();

        m_Animator.speed = 2.0f / (1.0f * (this.transform.localScale.x/20.0f));

        if (isGround == false)
        {
            if (!m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Jujo_Jump"))
            {
                m_Animator.SetBool("isGrounded", false);
            }

        }
        else
        {
            m_Animator.SetBool("isGrounded", true);
        }

        BlobShadow.GetComponent<Projector>().farClipPlane = this.transform.localScale.x * 1000;
        ChangeState();
        if (this.transform.localScale.x <= 15.0f)
        {
            DefaultSpeed = 9.0f;
        }
        else
        {
            DefaultSpeed = 14.0f;
        }
        if (this.transform.localScale.x <= 20.0f)
        {
            camera.GetComponent<CameraFollow>().offset.z = 5.71f * this.transform.localScale.x;
            camera.GetComponent<CameraFollow>().offset.y = 4.15f * this.transform.localScale.x;
        }
        else
        {
            camera.GetComponent<CameraFollow>().offset.z = (5.71f / 1.75f) * this.transform.localScale.x;
            camera.GetComponent<CameraFollow>().offset.y = 2.15f * this.transform.localScale.x;

        }

        GameManager.GetComponent<GameManager>().ObjectMovingTime = DefaultSize / this.transform.localScale.x;
        if (this.transform.localScale.x < 20.0f)
        {
            rb.drag = (DefaultSize - this.transform.localScale.x) * 0.08f;
        }
        else
        {
            rb.drag = 0.0f;
        }
        MovePlayer();

        if (this.transform.localScale.x >= 20)
        {
            if ((transform.position.y - PositionBeforeJumping) - transform.localScale.y >= sizeBeforeJumping + 10.0f)
            {
                rb.AddForce(-(Jumpforce / 15.0f) * Vector2.up);
            }
        }
        else
        {
            if ((transform.position.y - PositionBeforeJumping) - transform.localScale.y >= sizeBeforeJumping + 20.0f)
            {
                rb.AddForce(-(Jumpforce / 17.5f) * Vector2.up);
            }
        }

    }

    void JumpForceController() {
        if (this.transform.localScale.x <= 20 && this.transform.localScale.x >= 14)
        {
            Jumpforce = 1500.0f * 0.9f;
        }
        else if (this.transform.localScale.x <= 14)
        {
            Jumpforce = 1500.0f * 0.8f;
        }
        else
        {
            Jumpforce = (this.transform.localScale.x * 100) * 0.8f;
        }
    }



    void ChangeState()
    {
        if (disableInput)
        {
            return;
        }

        if (Input.GetMouseButton(0) || Input.GetMouseButton(1))
        {
            if (this.transform.localScale.x != SmallSize && this.transform.localScale.x != BigSize)
            {
                FindObjectOfType<SoundManager>().Play2("Shrink");
            }
            else
            {
                FindObjectOfType<SoundManager>().Stop("Shrink");
                transformation_Big_Effect.SetActive(false);
                transformation_Small_Effect.SetActive(false);
            }

        }

        if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1))
        {
            FindObjectOfType<SoundManager>().Stop("Shrink");
            transformation_Big_Effect.SetActive(false);
            transformation_Small_Effect.SetActive(false);
        }
        if (Input.GetMouseButton(0) != true && Input.GetMouseButton(1) != true)
        {

            FindObjectOfType<SoundManager>().Stop("Shrink");
            transformation_Big_Effect.SetActive(false);
            transformation_Small_Effect.SetActive(false);
        }

        if (Input.GetMouseButton(0))
        {
            transformation_Small_Effect.SetActive(true);
            shrinkPitch -= 0.025f;
            if (shrinkPitch <= 1.0f)
            {
                shrinkPitch = 1.0f;
            }
            FindObjectOfType<SoundManager>().changePitch("Shrink", shrinkPitch);

            walkPitch += 0.025f;
            if (walkPitch >= 3.0f)
            {
                walkPitch = 3.0f;
            }
            FindObjectOfType<SoundManager>().changePitch("Walk", walkPitch);

            if (this.transform.localScale.y <= SmallSize)
            {
                this.transform.localScale = new Vector3(SmallSize, SmallSize, SmallSize);
            }
            else
            {
                this.transform.localScale -= new Vector3(1.0f, 1.0f, 1.0f);
            }
        }
        if (Input.GetMouseButton(1))
        {
            transformation_Big_Effect.SetActive(true);
            shrinkPitch += 0.025f;
            if (shrinkPitch >= 3.0f)
            {
                shrinkPitch = 3.0f;
            }
            FindObjectOfType<SoundManager>().changePitch("Shrink", shrinkPitch);

            walkPitch -= 0.025f;
            if (walkPitch <= 1.0f)
            {
                walkPitch = 1.0f;
            }
            FindObjectOfType<SoundManager>().changePitch("Walk", walkPitch);
            if (this.transform.localScale.y >= BigSize)
            {
                this.transform.localScale = new Vector3(BigSize, BigSize, BigSize);
            }
            else
            {
                this.transform.localScale += new Vector3(1.0f, 1.0f, 1.0f);
            }


        }

    }

    void Jump()
    {
        if (disableInput) {
            return;
        }

        if (isGround == true)
        {
            jumplimit = 0;
        }

        if (Input.GetButtonDown("Jump") && jumplimit < 2)
        {
            PositionBeforeJumping = transform.position.y;
            sizeBeforeJumping = transform.localScale.y;
            m_MyAudioSource.Stop();
            FindObjectOfType<SoundManager>().Play("Jump");
            isGround = false;
            
            rb.AddForce(Jumpforce * Vector2.up);
            jumplimit++;
            
        }
    }

    void MovePlayer()
    {

        if (disableInput)
        {
            return;
        }

        float Speed = DefaultSpeed;

        float horiz = Input.GetAxis("Horizontal");
        float vert = Input.GetAxis("Vertical");

        if (isGround == false)
        {
            m_Animator.SetBool("isGrounded", false);
            Speed = 6.0f;
            Vector3 Velocity = new Vector3((-horiz) * Speed * Jumpforce * Time.fixedDeltaTime, rb.velocity.y, (-vert) * Speed * Jumpforce * Time.fixedDeltaTime);
            rb.velocity = Velocity;
        }
        else
        {
            m_Animator.SetBool("isGrounded", true);
            Vector3 Velocity = new Vector3((-horiz) * Speed * 1000 * Time.fixedDeltaTime, rb.velocity.y, (-vert) * Speed * 1000 * Time.fixedDeltaTime);
            rb.velocity = Velocity;
        }

        m_Animator.SetFloat("VeloX", rb.velocity.x);
        m_Animator.SetFloat("VeloY", rb.velocity.z);

    }

    public void Walk() {
        if (this.transform.localScale.x > DefaultSize + 10)
        {
            FindObjectOfType<SoundManager>().Play("GiantFootStep");
        }
        else {
            FindObjectOfType<SoundManager>().Play("Walk");
        }
        
    }


    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Ground" || col.gameObject.tag == "MovingPlatform" || col.gameObject.tag == "CheckPoint"|| col.gameObject.tag == "Goal")
        {
            isGround = true;
            OnAir = false;
        }

        if (col.gameObject.tag == "Laser" || col.gameObject.tag == "DeadlyFloor" || col.gameObject.tag == "DeadlyObstacle")
        {
            gameObject.SetActive(false);
        }
        if (col.gameObject.tag == "Goal")
        {
            VictoryUI.SetActive(true);
            GameManager.GetComponent<GameManager>().TimeStop = true;
            GameManager.GetComponent<GameManager>().CalculateScore = true;
        }
        if (col.gameObject.tag == "MovingPlatform")
        {
            transform.parent = col.transform;
        }

        if (col.gameObject.tag == "CheckPoint")
        {
            checkPoint = 1;
            spawnPosition = col.gameObject.transform.position;
            spawnPosition.y += 200.0f;
            //PlaySound.Check();
        }

    }

    void OnCollisionStay(Collision col)
    {


        if (col.gameObject.tag == "Goal")
        {
            VictoryUI.SetActive(true);
            disableInput = true;
            rb.velocity = new Vector3(0, 0, 0);
            m_Animator.SetFloat("VeloX", rb.velocity.x);
            m_Animator.SetFloat("VeloY", rb.velocity.z);
            OnAir = false;
        }

        if (col.gameObject.tag == "CheckPoint")
        {
            checkPoint = 1;
            spawnPosition = col.gameObject.transform.position;
            spawnPosition.y += 200.0f;
        }




    }

    void OnCollisionExit(Collision col)
    {
        if (col.gameObject.tag == "Ground" || col.gameObject.tag == "CheckPoint")
        {
            OnAir = true;
            m_Animator.SetBool("isGrounded", false);
        }
        if (col.gameObject.tag == "MovingPlatform")
        {
            transform.parent = null;
        }
    }
}
