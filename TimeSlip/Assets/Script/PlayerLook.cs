﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook : MonoBehaviour {

    public Transform playerBody;
    public float mouseSentivity;
    float xAxisClamp = 0.0f;
    // Use this for initialization
    void Awake () {
      //  Cursor.lockState = CursorLockMode.Locked;
	}
	
	// Update is called once per frame
	void Update () {
        RotateCamera();
    }

    void RotateCamera() {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        float rotAmountX = mouseX * mouseSentivity;
        float rotAmountY = mouseY * mouseSentivity;

        Vector3 targetRotCam = transform.rotation.eulerAngles;
        Vector3 targetRotBody = playerBody.rotation.eulerAngles;

        targetRotCam.x -= rotAmountY;
        if (targetRotCam.x > 20 && targetRotCam.x <= 50) {
            targetRotCam.x = 20.0f;
        }

        if (targetRotCam.x < 300 && targetRotCam.x >= 290)
        {
            targetRotCam.x = 300.0f;
        }



        targetRotCam.z = 0;
        targetRotBody.y += rotAmountX;

        if (xAxisClamp > 90) {
            xAxisClamp = 90;
            targetRotCam.x = 90;
        } else if (xAxisClamp < -90) {
            xAxisClamp = -90;
            targetRotCam.x = 270;
        }

        
        transform.rotation = Quaternion.Euler(targetRotCam);
        playerBody.rotation = Quaternion.Euler(targetRotBody);
    }
}
