﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shake : MonoBehaviour {
    public float speed = 1.0f; //how fast it shakes
    public float amount = 1.0f; //how much it shakes
    Vector3 startingPos;
    // Use this for initialization
    void Start () {
        startingPos.x = transform.position.x;
        startingPos.y = transform.position.y;
        startingPos.z = transform.position.z;
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(startingPos.x + (Mathf.Sin(Time.time * speed) * amount), startingPos.y + (Mathf.Sin(Time.time * speed) * amount), startingPos.z);
    }
}
