﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StageSelect : MonoBehaviour {
    int currentStage = 0;
    public GameObject [] StagePictures;
    public GameObject[] StageText;
    public GameObject[] LevelDetail;
    public GameObject PlayBTN;
    public string[] stageName;
    public Text stageTXT;
    public Text DescriptionTopicText;

    public GameObject btnField;

    public Animator anim;
    // Use this for initialization
    void Start () {
        DescriptionTopicText.gameObject.SetActive(false);
        PlayBTN.SetActive(false);
        btnField.SetActive(false);
        foreach (GameObject detail in LevelDetail)
        {
            detail.SetActive(false);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void selectStage(int numStage)
    {
        anim.Play("SelectAnyStage", -1, 0f);
        btnField.SetActive(true);
        DescriptionTopicText.gameObject.SetActive(true);
        PlayBTN.SetActive(true);
        currentStage = numStage;
        StagePictures[numStage-1].SetActive(true);
        LevelDetail[numStage - 1].SetActive(true);
        // StageText[numStage - 1].SetActive(true);
        stageTXT.text = stageName[numStage-1];

        for (int i = 0; i < StagePictures.Length; i++)
        {
            if (i != numStage - 1)
            {
                StagePictures[i].SetActive(false);
                LevelDetail[i].SetActive(false);
                //StageText[i].SetActive(false);
            }
        }
    }

    public void start() {
        if (currentStage == 1)
        {
            SceneManager.LoadScene("Stage 1-1");
        }
        else if (currentStage == 2)
        {
            SceneManager.LoadScene("Stage 1-2");
        }
        else if (currentStage == 3)
        {
            SceneManager.LoadScene("Stage 1-3");
        }
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
